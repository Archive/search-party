<?php

/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */

// xmlrpc includes
 
require_once("lib/xmlrpc_1.2/xmlrpc.inc");
require_once("lib/xmlrpc_1.2/xmlrpcs.inc");

// database includes

require_once("dbInit.php");

// Set up global data



// Method definitions

require_once("methods.php");


// Making the server

$s=new xmlrpc_server(array( 
			'searchparty.connect' => array(
				'function' => 'sp_connect',
				'signature'=> $sp_connect_sig,
				'docstring' => $sp_connect_doc),
               'searchparty.disconnect' => array(
                    'function' => 'sp_disconnect',
                    'signature' => $sp_disconnect_sig,
                    'docstring' => $sp_disconnect_doc),
               'searchparty.getUpdate' => array(
                    'function' => 'sp_getUpdate',
                    'signature' => $sp_getUpdate_sig,
                    'docstring' => $sp_getUpdate_doc),
               'searchparty.say' => array(
                    'function' => 'sp_say',
                    'signature' => $sp_say_sig,
                    'docstring' => $sp_say_doc),
               'searchparty.part' => array(
                    'function' => 'sp_part',
                    'signature' => $sp_part_sig,
                    'docstring' => $sp_part_doc),
               'searchparty.join' => array(
                    'function' => 'sp_join',
                    'signature' => $sp_join_sig,
                    'docstring' => $sp_join_doc),
               'searchparty.ping' => array(
                    'function' => 'sp_ping',
                    'signature' => $sp_ping_sig,
                    'docstring' => $sp_ping_doc),
               'searchparty.echoTest' => array(
                    'function' => 'sp_echoTest',
                    'signature' => $sp_echoTest_sig,
                    'docstring' => $sp_echoTest_doc),
               'searchparty.recommend' => array(
                    'function' => 'sp_recommend',
                    'signature' => $sp_recommend_sig,
                    'docstring' => $sp_recommend_doc),
               'searchparty.updateOptions' => array(
                    'function' => 'sp_updateOptions',
                    'signature' => $sp_updateOptions_sig,
                    'docstring' => $sp_updateOptions_doc),
               'searchparty.count' => array(
                    'function' => 'sp_count',
                    'signature' => $sp_count_sig,
                    'docstring' => $sp_count_doc)

			));               

?>			
			
