<?php

/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */
 
 
require_once("query-match.php");

// Common errors

$error_codes=array(
               0=>"Success!",
               1=>"There is an error with the supplied parameters.",
               2=>"Too many connections from this IP address.",
               3=>"The specified user name does not match the known IP address.",
               4=>"The specified chat room does not exist or the user is not currently connected to that room.",
               5=>"The server was unable to commit your message. Make sure your message is not malformed and try again.",
               6=>"",
               7=>"The requested user name was blank or contained invalid characters (such as ':')."
               );

               
// return room that matches a given query

function match_query($p_query)
{
     // trimming of query should be handled already
     
     
     $query = "SELECT id,name FROM sp_rooms WHERE name='$p_query'";
     $result = mysql_query($query);
     
     if($row = mysql_fetch_assoc($result))
     {
          $roomid = $row['id'];
          $roomname = $row['name'];
     }
     else
     {
          $joinval=.5;
          
          $query="SELECT id,name FROM sp_rooms";
          $result=mysql_query($query);
          
          $simroom=array('name'=>"",'id'=>"",'val'=>0);
          
          while($row=mysql_fetch_assoc($result))
          {
               $simval=compare_queries($p_query,$row['name']);
               if($simval > $simroom['val'])
               {
                    $simroom['val']=$simval;
                    $simroom['name']=$row['name'];
                    $simroom['id']=$row['id'];
               }
          }
          
          if($simroom['val'] < $joinval)
          {
               // create new room
               $query="INSERT INTO sp_rooms (name,chatBuffer) VALUES ('$p_query','')";
               mysql_query($query);
               $query="SELECT id,name FROM sp_rooms WHERE name='$p_query'";
               $result=mysql_query($query);
               $row=mysql_fetch_assoc($result);
               
               $roomname=$row['name'];
               $roomid=$row['id'];
          }
          else
          {
               $roomname=$simroom['name'];
               $roomid=$simroom['id'];
          }    
     }
     
     return array('name' => $roomname, 'id' => $roomid);
}
               
               
// This function gets latest chat/user info for a particular room and build an XML-RPC response
 
function getRoomStatus($theroom,$thekey="")
{
     global $xmlrpcStruct,$xmlrpcString,$xmlrpcArray;
     
     // Get options for each user
     
     $query="SELECT sp_users.name,sp_userlist.options FROM sp_userlist,sp_users WHERE sp_users.id=sp_userlist.user_id AND room_id=(SELECT id FROM sp_rooms WHERE name='$theroom')";
     $result=mysql_query($query);
     $userPrefs=array();
     while($row=mysql_fetch_assoc($result))
     {
          $userPrefs[$row['name']]=$row['options'];
     }
     
     // Build list of users and their queries to send to requester
     
     $query="SELECT sp_users.name,sp_userlist.query FROM sp_userlist,sp_users WHERE sp_users.id=sp_userlist.user_id AND room_id=(SELECT id FROM sp_rooms WHERE name='$theroom')";
     $result=mysql_query($query);
     $userList=array();
     while($row=mysql_fetch_array($result))
     {
          strstr($userPrefs[$row[0]], "NOSHOW")==false ? $userQuery=$row[1] : $userQuery="";
          $userList[]=new xmlrpcval($row[0] . ":" . $userQuery,$xmlrpcString);
     }
     $userArray=new xmlrpcval($userList,$xmlrpcArray);
     
     // Send back appropriate chat fragment
     
     $query="SELECT chatBuffer FROM sp_rooms WHERE name='$theroom'";
     $result=mysql_query($query);
     $row=mysql_fetch_array($result);
     $chatBuffer=$row[0];
     
     if($thekey!="")     // get most recent chat in chatBuffer
     {
          $chatBuffer="0" . $chatBuffer;     // put something at beginning to ensure $split[0] exists and is garbage
          $regexp="/<message id=\"$thekey\">.*?<\\/message>/ms";
          $split=preg_split($regexp,$chatBuffer,-1,PREG_SPLIT_NO_EMPTY);
          $chatBuffer=$split[1];
     }
     
     $currentChat = new xmlrpcval("<chatFragment>$chatBuffer</chatFragment>",$xmlrpcString);
     $roomName = new xmlrpcval($theroom,$xmlrpcString);
     
     // Return response as struct
     
     $returnStruct = new xmlrpcval(array("userList"=>$userArray, "currentChat"=>$currentChat, "roomName"=>$roomName), $xmlrpcStruct);
     return new xmlrpcresp($returnStruct);
}

function updateIdle($p_user)
{
     $query="UPDATE sp_users SET lastContact=" . gmdate("U") . " WHERE name='$p_user'";
     mysql_query($query);       
}

function checkIdle()
{
     $timeout="120";
     $unix_now=gmdate("U");
     
     $query="SELECT id FROM sp_users WHERE $unix_now - lastContact > $timeout";
     $result=mysql_query($query);
               
     while($row=mysql_fetch_row($result))
     {
          
          $id=$row[0];
          $query="DELETE FROM sp_userlist WHERE user_id=$id";
          mysql_query($query);
          $query="DELETE FROM sp_users WHERE id=$id";
          mysql_query($query);
     }   
}


// trim chatBuffer down based on size and age

function trim_buffer($chatBuffer)
{
     // need to account for age, too:
     //   1) Need to keep a buffer AT LEAST 3 seconds long (should keep 30 minutes or so?)
     //   2) Need to get rid of ancient stuff.
     
     $regexp = "/<message[^(?:<\\/message>)]*?/ms";
     
     if(strlen($chatBuffer) > 8000) // should be while, just testing
     {
          $split=preg_split($regexp,$chatBuffer,-1,PREG_SPLIT_NO_EMPTY);
          $split[0] = "";
          $chatBuffer= implode("<message",$split);
     }
     
     return $chatBuffer;
}


// sanitize/trim input

function clean_body($body)
{
     return substr(htmlspecialchars($body, ENT_QUOTES), 0, 512);
}

function clean_name($name)
{
     $invalid_chars = array(":");    // what other characters are not allowed?
     $name = str_replace($invalid_chars, "", $name);
     return substr(htmlspecialchars($name, ENT_QUOTES), 0, 25);
}

function clean_url($url)
{
     return substr(htmlspecialchars($url, ENT_QUOTES), 0, 250);   //improve this
}

function clean_title($title)
{
     return substr(htmlspecialchars($title, ENT_QUOTES), 0, 250);
}

function clean_query($query)
{
     return substr(htmlspecialchars($query, ENT_QUOTES), 0, 250); // 255 char limit
}

// should be identical to one above...

function clean_room($room)
{
     return substr(htmlspecialchars($room,ENT_QUOTES), 0, 250);
}

function clean_options($options)
{
     // do format checking, too!
     
     return substr(htmlspecialchars($options,ENT_QUOTES), 0, 250);
}


function clean_key($key)
{
     return substr(htmlspecialchars($key, ENT_QUOTES), 0, 250);   // improve this
}

?>
