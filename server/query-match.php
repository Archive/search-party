<?php

/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */
 

require_once("stem.php");

function stem_query($testquery)
{
     $testquery=strtolower($testquery);
	$tok=strtok($testquery, " ");
	
	$stopwords=array("a","the","for","to","and","if","but");
	$array=array();
	
	while($tok!==false)
	{
		$stem=PorterStemmer::Stem($tok);
		if(array_search($stem,$stopwords)===false && array_search($stem,$array)===false)	// check for stopwords (note: now query "and" ~= "" !!!)
			$array[] = $stem;
		$tok=strtok(" ");
	}
	
	sort($array);
	
	return $array;
}

function toString($array)
{
	$result="";
	
	foreach($array as $word)
		$result.=$word . " ";
		
	return $result;
}

function compare_stems($query1, $query2)
{
     $simval=0;
     $i=0;
     
     foreach($query1 as $word)
     {
          $i++;
          
          //if( is_numeric(array_search($word,$query2)) )
          //{
          //     $simval+=1.0;     // should actually be able to get partial points
          //}
          foreach($query2 as $comp_word)
          {
               if($word==$comp_word)
                    $simval+=1.0;
               else if(strstr($comp_word,$word))
                    $simval+=0.75;
               else if(substr($word,0,4)==substr($comp_word,0,4))
                    $simval+=0.5;
          }
     }
     
     if($i==0)
          return 0;
     
     $simval/=$i;
     
     return round($simval,3);
}

function compare_queries($query1,$query2)
{
     return
          ( compare_stems(stem_query($query1),stem_query($query2)) +
          compare_stems(stem_query($query2),stem_query($query1)) )
          /2;  // average the results
               
}

?>
