<?php

/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */
 

// internal functions
 
require_once("util.php");


//
// Defintions of XML-RPC methods
//

// Make a connection (use sp_join to actually join a chat)

$sp_connect_sig=array(array($xmlrpcString,$xmlrpcString));   // return string, just userName...?
$sp_connect_doc="Connect to a Search Party server with a given user name.";

function sp_connect($params)
{
     checkIdle();
     
     global $xmlrpcString;
     
     $error=0;

     $p_user = $params->getParam(0);
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_user = clean_name($p_user);
     $p_user == "" ? $error = 7 : $error = $error;   // invalid user name
     
     if($error==0)
     {
          $ip=$_SERVER["REMOTE_ADDR"];  // IP Address
          
          // Check to make sure $ip not already in use too many times

          $query="SELECT id FROM sp_users WHERE ip='$ip'";
          $result=mysql_query($query);
          if(mysql_num_rows($result)>5)
               $error=2; // IP already connected (greater than 5 times);
          else
          {               
               $query="SELECT name FROM sp_users WHERE name REGEXP '" . $p_user . "[0-9]{0,}'";
               $result=mysql_query($query);
               $count=mysql_num_rows($result);
               
               if($count==0)
                    $username=$p_user;
               else
               {
                    do
                    {
                         $username=$p_user . $count;
                         $query="SELECT name FROM sp_users WHERE name='$username'";
                         $result=mysql_query($query);
                         $count+=7;
                    }
                    while(mysql_num_rows($result)!=0);
               }
              
                    

               
               $query="INSERT INTO sp_users (name,ip,lastContact) VALUES ('$username','$ip'," . gmdate("U") . ")";
               
               mysql_query($query);
     
               
               // Build and send response
               
               return new xmlrpcresp(new xmlrpcval($username, $xmlrpcString));
          }
     }

     // if there was an error...
     
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);

}


// End a user's session

$sp_disconnect_sig=array(array($xmlrpcInt,$xmlrpcString));
$sp_disconnect_doc="End a user's session on the server, removing him or her from all chats.";

function sp_disconnect($params)
{
     global $xmlrpcInt;
     
     $error=0;
     
     $p_user=$params->getParam(0);
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_user = clean_name($p_user);
     
     $ip=$_SERVER["REMOTE_ADDR"];

     if($error==0)
     {
          // look up user, check against ID
          
          $query="SELECT id from sp_users WHERE name='$p_user' AND ip='$ip'";
          $result=mysql_query($query);
          
          if($row=mysql_fetch_row($result))
          {
               $id=$row[0];
               
               // make sure user is not in any chats, if so part from them
          
               $query="DELETE FROM sp_userlist WHERE user_id=$id";
               mysql_query($query);
               $query="DELETE FROM sp_users WHERE id=$id";
               mysql_query($query);
          
               // delete user
          
               $status=1;     // means success
               
               return new xmlrpcresp(new xmlrpcval($status,$xmlrpcInt));
          }
          else
               $error=3; // IP mismatch
     }

     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);
    
}


// Have a user quit a particular chat

$sp_part_sig=array(array($xmlrpcInt,$xmlrpcString,$xmlrpcString));
$sp_part_doc="Quits a user from a given chat, but leaves their session open (use searchparty.disconnect to close a session)";

function sp_part($params)
{
     global $xmlrpcInt;
     
     $error=0;
     
     $p_user=$params->getParam(0);
     $p_room=$params->getParam(1);
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_room->kindOf()=="scalar" ? $p_room=$p_room->scalarval() : $error=1;
     
     $p_user = clean_name($p_user);
     $p_room = clean_room($p_room);
     
     $ip=$_SERVER["REMOTE_ADDR"];

     if($error==0)
     {
          // look up user, check against ID
          
          $query="SELECT id from sp_users WHERE name='$p_user' AND ip='$ip'";
          $result=mysql_query($query);
          
          if($row=mysql_fetch_row($result))
          {
               $userid=$row[0];
               
               $query="DELETE FROM sp_userlist WHERE user_id=$userid AND room_id=(SELECT id FROM sp_rooms WHERE name='$p_room')";
               mysql_query($query);

               if(mysql_affected_rows()==1)
                    $status=1;     // means success
               else
                    $status=2;     // means user was never in room
               
               return new xmlrpcresp(new xmlrpcval($status,$xmlrpcInt));
          }
          else
               $error=3; // ip mismatch
     }
     
     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);        
}



// Have a user join a particular chat

$sp_join_sig=array(array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString),array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString));
$sp_join_doc="Joins an already connected user to a chat based on a given query, with given options.  Optional fourth parameter is for 'last message id received'.";

function sp_join($params)
{
     global $xmlrpcString,$xmlrpcStruct;     // should I use an array for userList or latestChat?
     
     $error=0;
     
     $p_user=$params->getParam(0);
     $p_query=$params->getParam(1);
     $p_options=$params->getParam(2);
     
     if($params->getNumParams()==4)
     {
          $p_key=$params->getParam(3);
          $p_key->kindOf()=="scalar" ? $p_key=$p_key->scalarval() : $error=1;
     }
     else
          $p_key="";
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_query->kindOf()=="scalar" ? $p_query=$p_query->scalarval() : $error=1;
     $p_options->kindOf()=="scalar" ? $p_options=$p_options->scalarval() : $error=1;
     
     $p_user = clean_name($p_user);
     $p_query = clean_query($p_query);
     $p_options = clean_options($p_options);  // does any real ERROR checking/reporting need to happen here?
     
     $ip=$_SERVER["REMOTE_ADDR"];

     if($error==0)
     {
          // look up user, check against ID
          
          $query="SELECT id from sp_users WHERE name='$p_user' AND ip='$ip'";
          $result=mysql_query($query);
          
          if($row=mysql_fetch_row($result))
          {
               updateIdle($p_user);
               
               $userid=$row[0];
               
               // Do query matching to choose room.  Make a new one if necessary (this is currently bs, of course)
               
               $roomInfo = match_query($p_query);
               $roomid = $roomInfo['id'];
               $roomname = $roomInfo['name'];               
               
               // Put user in room
               
               $query="INSERT INTO sp_userlist (user_id,room_id,query,options) VALUES ($userid,$roomid,'$p_query','$p_options')";
               mysql_query($query);
               
               // Return userlist and latest chat
               
               return getRoomStatus($roomname,$p_key);
          }
          else
               $error=3; // ip mismatch
 

     }

     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);
}


// Keep connection alive

$sp_ping_sig=array(array($xmlrpcInt,$xmlrpcString));
$sp_ping_doc="Pings the server to keep a user's connection alive while in between chats";

function sp_ping($params)
{
     global $xmlrpcInt;
     
     $error=0;
     
     $p_user=$params->getParam(0);
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_user = clean_name($p_user);
     
     $ip=$_SERVER["REMOTE_ADDR"];

     if($error==0)
     {
          // look up user, check against ID
          
          $query="SELECT id from sp_users WHERE name='$p_user' AND ip='$ip'";
          $result=mysql_query($query);
          
          if($row=mysql_fetch_row($result))
          {
               $userid=$row[0];
               
               $query="UPDATE sp_users SET lastContact=" . gmdate("U") . " WHERE id=$userid";
               mysql_query($query);
               
               $status=1;     // means success
               
               return new xmlrpcresp(new xmlrpcval($status,$xmlrpcInt));
          }
          else
               $error=3; // ip mismatch
     }

     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);     
}



// Update user's options

$sp_updateOptions_sig=array(array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString),array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString));
$sp_updateOptions_doc="Change the user's option string for a given room.  Optional fourth parameter is for 'last message id received'.";

function sp_updateOptions($params)
{
     global $xmlrpcStruct,$xmlrpcArray,$xmlrpcString;
     
     $error=0;
     
     $p_user=$params->getParam(0);
     $p_room=$params->getParam(1);
     $p_options=$params->getParam(2);
          
     if($params->getNumParams()==4)
     {
          $p_key=$params->getParam(3);
          $p_key->kindOf()=="scalar" ? $p_key=$p_key->scalarval() : $error=1;
          $p_key = clean_key($p_key);
     }
     else
          $p_key="";
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_room->kindOf()=="scalar" ? $p_room=$p_room->scalarval() : $error=1;
     $p_options->kindOf()=="scalar" ? $p_options=$p_options->scalarval() : $error=1;
     
     $p_user = clean_name($p_user);
     $p_room = clean_room($p_room);
     $p_options = clean_options($p_options);
     
     $ip=$_SERVER["REMOTE_ADDR"];

     if($error==0)
     {
          // look up user, check against ID
          
          $query="SELECT id from sp_users WHERE name='$p_user' AND ip='$ip'";
          $result=mysql_query($query);
          
          if($row=mysql_fetch_row($result))
          {
               updateIdle($p_user);
               
               $userid=$row[0];
               
               $query="UPDATE sp_userlist SET options='$p_options' WHERE user_id=$userid AND room_id=(SELECT id FROM sp_rooms WHERE name='$p_room')";
               mysql_query($query);
               
               if(mysql_affected_rows()==1)
               {
                    return getRoomStatus($p_room,$p_key);
               }
               else
                    $error=4; // invalid room or user not in room

          }
          else
               $error=3; // ip mismatch
          
     }

     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);     
}  
     


// Send a message

$sp_say_sig=array(array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString),array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString));
$sp_say_doc="Sends a user's message to a particular room.  Optional fourth parameter is for 'last message id received'.";

function sp_say($params)
{
     global $xmlrpcStruct,$xmlrpcArray,$xmlrpcString;
     
     $error=0;
     
     $p_user=$params->getParam(0);
     $p_room=$params->getParam(1);
     $p_message=$params->getParam(2);
     
     if($params->getNumParams()==4)
     {
          $p_key=$params->getParam(3);
          $p_key->kindOf()=="scalar" ? $p_key=$p_key->scalarval() : $error=1;
          $p_key = clean_key($p_key);
     }
     else
          $p_key="";
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_room->kindOf()=="scalar" ? $p_room=$p_room->scalarval() : $error=1;
     $p_message->kindOf()=="scalar" ? $p_message=$p_message->scalarval() : $error=1;
     
     $p_user = clean_name($p_user);
     $p_room = clean_room($p_room);
     $p_message = clean_body($p_message);
     
     $ip=$_SERVER["REMOTE_ADDR"];

     if($error==0)
     {
          // look up user, check against ip and make sure user is in room, retreive user id and chat buffer
          
          $query="SELECT sp_users.id,sp_rooms.chatBuffer from sp_users,sp_userlist,sp_rooms " .
                                                                 "WHERE sp_users.name='$p_user' AND sp_users.ip='$ip' " .
                                                                 "AND sp_rooms.name='$p_room' AND sp_rooms.id=sp_userlist.room_id " .
                                                                 "AND sp_users.id=sp_userlist.user_id";
          $result=mysql_query($query);
          
          if($row=mysql_fetch_row($result))
          {
               updateIdle($p_user);
               
               $userid = $row[0];
               $chatBuffer = $row[1];
               
               // this length-checking code only checks length, not age
               
               //if($p_room == "ogle")    // for testing
                    $chatBuffer = trim_buffer($chatBuffer); // trim chatBuffer xml to 8000 chars
               
               
               // commit new message to database
               
               $time=gmdate("U");
               $microtime=microtime();
               
               $query="UPDATE sp_rooms SET chatBuffer='" . $chatBuffer . "<message id=\"$p_user$microtime\"><sender>$p_user</sender><time>$time</time><url/><body>$p_message</body></message>' WHERE name='$p_room'";
               mysql_query($query);
                                        
               if(mysql_affected_rows()==1)
                    return getRoomStatus($p_room,$p_key);
               else
                    $error=5; // the server was unable to commit your message

          }
          else
               $error=3; // ip mismatch or user not in room or room doesn't exist
          
     }

     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);     
}


// Get latest room info

$sp_getUpdate_sig=array(array($xmlrpcStruct,$xmlrpcString),array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString));
$sp_getUpdate_doc="Get latest chat and member info for a given room.  Can take a room name as the sole argument, or the triple (user,roomname,lastMessageId).  'lastMessageId' should be an empty string if you just want all the available chat updates.";

function sp_getUpdate($params)
{
     global $xmlrpcStruct,$xmlrpcArray,$xmlrpcString;
     
     $error=0;
     
     $numParams=$params->getNumParams();
     
     if($numParams==3)
     {
          $p_user=$params->getParam(0);
          $p_room=$params->getParam(1);
          $p_key=$params->getParam(2);
          $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
          $p_key->kindOf()=="scalar" ? $p_key=$p_key->scalarval() : $error=1;
          $p_room->kindOf()=="scalar" ? $p_room=$p_room->scalarval() : $error=1;
          
          $p_user = clean_name($p_user);
          $p_key = clean_key($p_key);
          $p_room = clean_room($p_room);
          
          $roomname = $p_room;
     }
     else
     {
          $p_user="";
          $p_room=$params->getParam(0);
          $p_room->kindOf()=="scalar" ? $p_room =$p_room->scalarval() : $error=1;
          $p_room = clean_room($p_room);
          $p_key="";          
                   
          $roomInfo = match_query($p_room);
          $roomname = $roomInfo['name'];
     }
     
     
     
     $ip=$_SERVER["REMOTE_ADDR"];

     // Need to check that room EXISTS!
     
     if($error==0)
     {
          if($p_user!="")     // no point in checking if user is connected, since select probably costs same as failed update
               updateIdle($p_user);
          
          // no need to check ip...
          
          return getRoomStatus($roomname,$p_key);
     }

     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"Error code $error : " . $error_codes[$error]);     
}


// Send a recommendation

$sp_recommend_sig=array(array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString),array($xmlrpcStruct,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString,$xmlrpcString));
$sp_recommend_doc="Sends a user's recommendation to a particular room.  Parameters are (username,roomname,url,title,message).  Optional sixth parameter is for 'last message id received'.";

function sp_recommend($params)
{
     global $xmlrpcStruct,$xmlrpcArray,$xmlrpcString;
     
     $error=0;
     
     $p_user=$params->getParam(0);
     $p_room=$params->getParam(1);
     $p_url=$params->getParam(2);
     $p_title=$params->getParam(3);
     $p_message=$params->getParam(4);
     
     if($params->getNumParams()==6)
     {
          $p_key=$params->getParam(5);
          $p_key->kindOf()=="scalar" ? $p_key=$p_key->scalarval() : $error=1;
          $p_key = clean_key($p_key);
     }
     else
          $p_key="";
     
     $p_user->kindOf()=="scalar" ? $p_user=$p_user->scalarval() : $error=1;
     $p_room->kindOf()=="scalar" ? $p_room=$p_room->scalarval() : $error=1;
     $p_url->kindOf()=="scalar" ? $p_url=$p_url->scalarval() : $error=1;
     $p_title->kindOf()=="scalar" ? $p_title=$p_title->scalarval() : $error=1;
     $p_message->kindOf()=="scalar" ? $p_message=$p_message->scalarval() : $error=1;
     
     $p_user = clean_name($p_user);
     $p_room = clean_room($p_room);
     $p_url = clean_url($p_url);
     $p_title = clean_title($p_title);
     $p_message = clean_body($p_message);
     
     $ip=$_SERVER["REMOTE_ADDR"];

     if($error==0)
     {
          // look up user, check against ip and make sure user is in room, retreive user id and chat buffer
          
          $query="SELECT sp_users.id,sp_rooms.chatBuffer from sp_users,sp_userlist,sp_rooms " .
                                                                 "WHERE sp_users.name='$p_user' AND sp_users.ip='$ip' " .
                                                                 "AND sp_rooms.name='$p_room' AND sp_rooms.id=sp_userlist.room_id " .
                                                                 "AND sp_users.id=sp_userlist.user_id";
          $result=mysql_query($query);
          
          if($row=mysql_fetch_row($result))
          {
               updateIdle($p_user);
               
               $userid = $row[0];
               $chatBuffer = $row[1];
                    
               // commit new message to database
               
               $time=gmdate("U");
               $microtime=microtime();
               
               $query="UPDATE sp_rooms SET chatBuffer='" . $chatBuffer . "<message id=\"$p_user$microtime\"><sender>$p_user</sender><time>$time</time><url title=\\\"$p_title\\\">$p_url</url><body>$p_message</body></message>' WHERE name='$p_room'";
               mysql_query($query);
                                        
               if(mysql_affected_rows()==1)
                    return getRoomStatus($p_room,$p_key);
               else
                    $error=5; // server couldn't commit your message
          }
          else
               $error=3; // ip mismatch or user not in room or room doesn't exist
          
     }

     // if there was an error...
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"searchparty.recommend() : Error code $error : " . $error_codes[$error]);     
}


// Get number of users in a room about a given query

$sp_count_sig=array(array($xmlrpcInt,$xmlrpcString));
$sp_count_doc="Returns the number of users in the room to which a given query would match.";

function sp_count($params)
{
     global $xmlrpcInt,$xmlrpcString;
     
     $error=0;
     
     $p_room=$params->getParam(0);
     $p_room->kindOf()=="scalar" ? $p_room=$p_room->scalarval() : $error=1;
     $p_room = clean_room($p_room);
          
     if($error == 0)
     {
          $roomInfo = match_query($p_room);
          $roomid = $roomInfo['id'];
          
          $query = "SELECT user_id FROM sp_userlist WHERE room_id=$roomid";
          $result = mysql_query($query);
          $count = mysql_num_rows($result);
          
          $returnInt = new xmlrpcval($count, $xmlrpcInt);
          return new xmlrpcresp($returnInt);
     }
          
     // if there was an error     
     return new xmlrpcresp(0,$xmlrpcerruser+$error,"searchparty.count() : Error code $error : " . $error_codes[$error]);
}


// Just for testing...something like echo

$sp_echoTest_sig=array(array($xmlrpString,$xmlrpcString));
$sp_echoTest_doc="echoes back 'you said:MESSAGE'";

function sp_echoTest($params)
{
     global $xmlrpcString;
     
     $p_mesg = $params->getParam(0);
     
     $p_mesg="you said: " . clean_body($p_mesg->scalarval());
     
     return new xmlrpcresp(new xmlrpcval($p_mesg, $xmlrpcString));     
}


?>
