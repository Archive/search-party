<?php

/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */
 
require_once("query-match.php");

if(isset($_POST["query1"]))
	$firstquery=$_POST["query1"];
else
	$firstquery="";
	
if(isset($_POST["query2"]))
	$secondquery=$_POST["query2"];
else
	$secondquery="";

?>

<html>

<head><title>Testing Query Matching</title></head>

<body>

<h1>Query Matching Test Form</h1>

<form method="post" action="stem-test.php">
	First Query: <input type="text" name="query1" size="30" value="<?php echo $firstquery ?>" /><br />
	Second Query: <input type="text" name="query2" size="30" value="<?php echo $secondquery ?>" /><br />
	<input type="submit" value="Compare!" />
</form><br />

<?php

$q1_stemmed=stem_query($firstquery);
$q2_stemmed=stem_query($secondquery);

echo "\n<b>" . toString($q1_stemmed) . "<----- vs -----> " . toString($q2_stemmed) . "<br />\n";

$simval1=compare_stems($q1_stemmed,$q2_stemmed);
$simval2=compare_stems($q2_stemmed,$q1_stemmed);

echo /*max($simval1,$simval2)*/compare_queries($firstquery,$secondquery)*100 . "% Similar</b><br />\n";

?>

<p>The algorithm is currently as such:</p>

<ol>
     <li>Convert queries to all lowercase.</li>
     <li>Remove some stopwords (like and, the, but) -- I should probably only remove stopwords if the query is over a certain length?</li>
     <li>Apply Porter stemming algorithm to each word.</li>
     <li>Convert query into an array of stems (duplicates removed and forgotten)</li>
     <li>Compare the stems in each query like so:
          <ul>
               <li>If the stems are identical, the similarity value is incremented by 1.</li>
               <li>If one stem is contained in the other, the similarity value is incremented by 0.75.</li>
               <li>If the first four characters of the stems are identical, the similarity value is incremented by 0.5</li>
          </ul>    
     </li>
     <li>The similarity value is then divided by the number of stems to arrive at a percent similar value</li>
</ol>

<p>Check back for improvements.</p>

</body>

</html>



