/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */


var initialized,spWindow,chatWindow,recWindow,wm,isActive,query;

window.addEventListener("load", function() { myExtension.init(); }, false);
var prefs=Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.searchparty.");


var myExtension = {
     init: function() {
          window.addEventListener("close",this.onClose, true);
          initialized = false;
          isActive = false;
          spWindow = window;
          wm = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);
          var appcontent = document.getElementById("appcontent");   // browser
          if(appcontent)
               appcontent.addEventListener("load", this.onPageLoad, true);   
     },
     
     onClose: function(aEvent) {
          if(isActive)
          {
               if(window.confirm("Closing this window will cause your Search Party chat to close, too.\nAre you sure you want to close this window?"))
               {
                    chatWindow.closeChatWindow();
                    chatWindow.close();
                    catchClose();
               }
               else
                    aEvent.preventDefault();                             
          }
     },

     onPageLoad: function(aEvent) {
          var doc = aEvent.originalTarget; // doc is document that triggered "onload" event

          var url=doc.location.href;
          
          if(!initialized)         // new window?
          {
               // initialize   
               
               var enumerator = wm.getEnumerator("navigator:browser");
               while(enumerator.hasMoreElements())
               {
                    var win = enumerator.getNext();
                    var theirButton
                    if(theirButton = win.document.getElementById("sp-button"))
                    {
                         if(win.isActive)
                              spWindow = win;                         
                         
                         tempContainer = document.createElement("toolbaritem");
                         tempContainer.setAttribute("id","sp-container");
                         
                         tempButton = document.createElement("toolbarbutton");
                         tempButton.setAttribute("id","sp-button");
                         tempButton.setAttribute("label",theirButton.getAttribute("label"));
                         tempButton.setAttribute("tooltiptext",theirButton.getAttribute("tooltiptext"));
                         tempButton.setAttribute("oncommand",theirButton.getAttribute("oncommand"));
                         
                         tempContainer.appendChild(tempButton);
                         document.getElementById("nav-bar").insertBefore(tempContainer, document.getElementById("fullscreenflex"));
                         
                         break;
                    }
               } 
               initialized = true;
          }
          else if(spWindow.closed || !spWindow.isActive)
               spWindow = window;
          
          if(spWindow == window && !isActive)
          {
               // do google checking, etc
               if(url.search("www.google") > -1)
               {
                    d = new Date();
                    var thetime=d.valueOf().toString();
                    
                    // prepare query string
                    
                    var result=url.match(/[\?&]q=([^&]+)/);
                    var query=result[1].replace(/\+/g," ");
                    
                    // some variables
                    
                    var navBar,spButton,xmlrpc,server,userCount,tempButton,tempContainer;
                    
                    // get a user count
                    
                    try
                    {
                         xmlrpc = importModule("xmlrpc");
                         server = new xmlrpc.ServerProxy(prefs.getCharPref("server"));
                    }
                    catch(e)
                    {
                         reportException(e);
                         throw "importing of xmlrpc module failed.";
                    }
                                   
                    try
                    {
                         userCount=server.searchparty.count(query);
                    }
                    catch(e)
                    {
                         reportException(e);
                    }
                    
                    if(!userCount)
                         userCount = 0;
                    
                    // tell every window (including this one!)                    
                    var enumerator = wm.getEnumerator("navigator:browser");
                    while(enumerator.hasMoreElements())
                    {
                         var win = enumerator.getNext();
                         
                         // modify button for win
                         
                         navBar = win.document.getElementById("nav-bar");
                         spButton = null;
                         
                         if(spButton = win.document.getElementById("sp-button"))
                         {
                              //if(spButton.getAttribute("label")!="Recommend to Search Party")
                                   navBar.removeChild(win.document.getElementById("sp-container"));  // not the best place for this...
                              //else
                              //     break;   // there's already a recommend button!
                         }
                         
                         tempContainer=win.document.createElement("toolbaritem");
                         tempContainer.setAttribute("id","sp-container");
                         
                         tempButton=win.document.createElement("toolbarbutton");
                         tempButton.setAttribute("id","sp-button");
                         tempButton.setAttribute("oncommand","joinChat('"+query+"');");
                         tempButton.setAttribute("label", "Join Search Party! (" + userCount + " people)");              
                         tempButton.setAttribute("tooltiptext","Join Search Party for '" + query + "'");
                         
                         tempContainer.appendChild(tempButton);
                         navBar.insertBefore(tempContainer, win.document.getElementById("fullscreenflex"));
                    }
               }
          }
          else
               ;    // don't!



     }
}

function joinChat(p_query)
{
     // among other things
     isActive = true;

     if(prefs.getBoolPref("setprefs") == true)
     {
          query = p_query;
          
          spWindow.chatWindow = window.open("chrome://searchparty/content/chatWindow.xul","Search Party - " + query,"chrome,width=650,height=450");
     
          spWindow.chatWindow.addEventListener("load", function() { spWindow.chatWindow.loadChatWindow(query); }, false);
          spWindow.chatWindow.addEventListener("close", function() { catchClose(); }, false);
          
          // tell every window (including this one!)                    
          var enumerator = wm.getEnumerator("navigator:browser");
          while(enumerator.hasMoreElements())
          {
               var win = enumerator.getNext();
               
               win.spWindow = window;
               var qbutton = win.document.getElementById("sp-button");
               qbutton.setAttribute("label","Recommend to Search Party");
               qbutton.setAttribute("tooltiptext","Recommend this site in Search Party for '" + query + "'");
               qbutton.setAttribute("oncommand","recommend();");
          }
     }
     else
          window.open("chrome://searchparty/content/options.xul","options","chrome");
}

function recommend()
{
     if(spWindow.chatWindow && spWindow.chatWindow.connectedFlag==true)
     {
          var doc = gBrowser.getBrowserForTab(gBrowser.selectedTab).contentDocument;   // best way?
          
          recWindow = window.open("chrome://searchparty/content/recWindow.xul","recwin","chrome,width=425,height=175");
          recWindow.addEventListener("load", function() {recWindow.initWindow(doc.location.href, doc.title, spWindow.chatWindow); }, false);        
     }
     else
          alert("There is no active Search Party chat to accept recommendations.\nPerform a search at google.com and click 'Join Search Party!'");
}

function catchClose()
{
     isActive = false;
     // tell every window (including this one!)                    
     var enumerator = wm.getEnumerator("navigator:browser");
     while(enumerator.hasMoreElements())
     {
          var win = enumerator.getNext();
          var navBar=win.document.getElementById("nav-bar");
          if(navBar)
          {
               navBar.removeChild(win.document.getElementById("sp-container"));
               win.chatWindow=null;
          }
     }
}


