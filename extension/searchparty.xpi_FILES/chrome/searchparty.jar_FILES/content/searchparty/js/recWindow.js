/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */


// global variables

var spWindow;
var commentBox,recLink,recLabel;


function OK_clicked()
{
     spWindow.recommend(recLink.href, recLink.title, commentBox.value);
     close();
}

function cancel_clicked()
{
     close();
}

function initWindow(url,title,chatWindow)
{
     commentBox=document.getElementById("commentBox");
     recLabel=document.getElementById("recLabel");
     recLink=document.getElementById("recLink");
     
     recLink.title = title;
     if(title.length > 30)
          title = title.substr(0,40) + "...";
     recLink.href=url;
     recLabel.value=title;
     
     spWindow=chatWindow;
     
     commentBox.focus();
}
