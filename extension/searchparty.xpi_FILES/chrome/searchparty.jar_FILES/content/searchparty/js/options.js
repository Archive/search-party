/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */
 

var prefs,server,options,username;
var optionsUsername,optionsServer,optionsShare;
 
 function loadOptionsWindow()
 {
      // initialize global variables
      
      prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.searchparty.");
      server = prefs.getCharPref("server");
      options = prefs.getCharPref("options");
      username = prefs.getCharPref("username");
      preview = prefs.getBoolPref("preview");
      
      optionsUsername = document.getElementById("options-username");
      optionsServer = document.getElementById("options-server");
      optionsShare = document.getElementById("options-share");
      optionsPreview = document.getElementById("options-preview");
      
      // end initialization
      
      optionsUsername.value=username;
      optionsServer.value=server;
      options == "NOSHOW" ? optionsShare.checked = false : optionsShare.checked = true;
      preview == true ? optionsPreview.checked = true : optionsPreview.checked = false;
 }
 
 function closeOptionsWindow()
 {
      prefs.setCharPref("username",optionsUsername.value);
      prefs.setCharPref("server",optionsServer.value);
      optionsShare.checked == true ? prefs.setCharPref("options","") : prefs.setCharPref("options","NOSHOW");
      optionsPreview.checked == true ? prefs.setBoolPref("preview",true) : prefs.setBoolPref("preview",false);
      prefs.setBoolPref("setprefs",true);
      window.close();
 }
 
 function resetOptions() // this should actually be grabbed from searchparty.js, right?
 {
      optionsUsername.value = "user name";
      optionsServer.value = "http://www.searchpartyproject.com/server2.php";
      optionsShare.checked = true;
      optionsPreview.checked = true;
 }
