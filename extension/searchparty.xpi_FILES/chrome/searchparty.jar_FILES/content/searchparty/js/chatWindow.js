/*
 *  Search Party
 *  Copyright (C) 2005  Sanford Armstrong
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *   
 *  Please direct questions about Search Party to Sandy at
 *  sanfordarmstrong@gmail.com.
 */

// Global variables
 
var chatbox;

var saybox;
var buddybox;

var query;
var roomName;

var lastId;

var timer;

var xmlrpc; 
var server;
var username;

var prefs;
var serverURL;
var options;

var userArray;

var connectedFlag;
     

// Most server methods return room status.  This function handles those responses.

function handleRoomStatus(roomStatus)
{
     clearTimeout(timer);
     
     var userList = roomStatus["userList"];
     var currentChatXML = roomStatus["currentChat"];
     
     if(currentChatXML!="<chatFragment></chatFragment>")
     {     
          // parsing
          var parser=new DOMParser();
          var currentChat = parser.parseFromString(currentChatXML, "text/xml");
          var messages=currentChat.getElementsByTagName("message");
          var m,sender,time,bodyNode,body,urlNode;
          for(var i=0; i<messages.length; i++)
          {
               m = messages[i];
               sender = m.getElementsByTagName("sender")[0].firstChild.data;
               time = m.getElementsByTagName("time")[0].firstChild.data;
               bodyNode = m.getElementsByTagName("body")[0];
               
               if(bodyNode && bodyNode.hasChildNodes())
               {
                    body = bodyNode.firstChild.data;
               }
               else
                    body = "";
               
               urlNode=m.getElementsByTagName("url")[0];
               
               var bodyText,senderText,boxclass;
               
               var bodySpan = chatbox.createElement("span");
               bodySpan.setAttribute("class","body");
               
                              
               if(urlNode && urlNode.hasChildNodes())
               {
                    boxclass="recommendation";
                    
                    var url = urlNode.firstChild.data;
                    var title = urlNode.getAttribute("title");
                    
                    //var recMsg = "<a href=\"" + url + "\">" + title + "</a>";
                    var link = chatbox.createElement("a");
                    link.setAttribute("href",url);
                    link.setAttribute("target","_blank");
                    link.appendChild(chatbox.createTextNode(title));
                    bodySpan.appendChild(link);
                    
                    senderText = chatbox.createTextNode(sender + " recommends:");
                    bodyText = chatbox.createTextNode(" : " + body);
               }
               else
               {
                    boxclass="message";
                    
                    bodyText = chatbox.createTextNode(body);
                    senderText = chatbox.createTextNode(sender + ":");
               }
               
               //var bodyText = chatbox.createTextNode(body);
               //var bodySpan = chatbox.createElement("span");
               //bodySpan.setAttribute("class","body");
               
               bodySpan.appendChild(bodyText);
               
               //var senderText = chatbox.createTextNode(sender + ":");
               var senderSpan = chatbox.createElement("span");
               senderSpan.setAttribute("class","sender");
               senderSpan.appendChild(senderText);
               
               var messageDiv = chatbox.createElement("div");
               messageDiv.setAttribute("class",boxclass + hash(sender));
               messageDiv.appendChild(senderSpan);
               messageDiv.appendChild(bodySpan);
               
               chatbox.getElementById("msgbox").appendChild(messageDiv);
          }
          
          lastId=m.getAttribute("id");
          
          document.getElementById("chatbox").contentWindow.scrollBy(0,chatbox.height);
     }     
     
     var buddyboxBody=buddybox.getElementsByTagName("body")[0];
     buddyboxBody.removeChild(buddybox.getElementById("userbox"));
     var userbox = buddybox.createElement("div");
     userbox.setAttribute("id","userbox");
     buddyboxBody.appendChild(userbox);
     
     if(userList != userArray)
     {
          userArray = userList;
          
          for(var i=0; i<userList.length; i++)
          {             
               var userEntry = userList[i].split(":");
               
               var nameText = buddybox.createTextNode(userEntry[0]);
               var nameSpan = buddybox.createElement("span");
               nameSpan.setAttribute("class","name");
               nameSpan.appendChild(nameText);
               
               var queryText = buddybox.createTextNode(userEntry[1]);
               var querySpan = buddybox.createElement("span");
               querySpan.setAttribute("class","query");
               querySpan.appendChild(queryText);
               
               var userDiv = buddybox.createElement("div");
               userDiv.setAttribute("class","user" + hash(userEntry[0]));
               userDiv.appendChild(nameSpan);
               userDiv.appendChild(querySpan);
               userbox.appendChild(userDiv);
          }
     }
     
     timer=setTimeout("refreshChat()", 3000);     // get new update every 3 seconds
}


function loadChatWindow(p_query)
{
     connectedFlag = false;
     query = p_query;
     
     // INIT
     
     chatbox=document.getElementById('chatbox').contentDocument;
     
     buddybox=document.getElementById('peeps').contentDocument;
          
     lastId="";
     
     timer=null;
          
     prefs=Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.searchparty.");
     serverURL=prefs.getCharPref("server");
     options=prefs.getCharPref("options");     // Currently only option is NOSHOW
     var preview = prefs.getBoolPref("preview");
          
     try
     {
          xmlrpc = importModule("xmlrpc");
          server = new xmlrpc.ServerProxy(serverURL);
     }
     catch(e)
     {
          reportException(e);
          throw "importing of xmlrpc module failed.";
          window.close();
     }
     
     // END INIT
          
     if(preview == true)
     {
          
          var status;
          
          try
          {
               status=server.searchparty.getUpdate(query);
          }
               catch(e)
          {
               reportException(e);
               window.close();
          }
          
          roomName=status["roomName"];
          
          document.getElementById("searchparty-window").setAttribute("title","Search Party -- "+roomName);
          
          handleRoomStatus(status);
          
          clearTimeout(timer);
     }
     else
          joinChat();

}

function joinChat()	// should not have this here... (why did I write this?  I forget...)
{
     // Add proper XUL stuff
     
     var lowerBox = document.getElementById('lowerBox');
     var mainBox = document.getElementById('mainBox');
     
     mainBox.removeChild(lowerBox);
     lowerBox = document.createElement('vbox');
     lowerBox.setAttribute('id','lowerBox');
     
     saybox = document.createElement('textbox');
     saybox.setAttribute('id','saybox');
     saybox.setAttribute('multiline','false');
     saybox.setAttribute('flex','1');
     saybox.setAttribute('onkeypress','if(event.keyCode == 13){talk();}');
     saybox.setAttribute('tabindex','1');
     
     var saybutton = document.createElement('button');
     saybutton.setAttribute('id','saybutton');
     saybutton.setAttribute('label','Say It!');
     saybutton.setAttribute('oncommand','talk()');
     saybutton.setAttribute('tabindex','2');
     
     var shareq = document.createElement('checkbox');
     shareq.setAttribute('id','shareq');
     if(options == "NOSHOW")
          shareq.setAttribute('checked','false');
     else
          shareq.setAttribute('checked','true');
     shareq.setAttribute('label',"Share what you're currently searching for");
     shareq.setAttribute('oncommand','shareState();');
     shareq.setAttribute('tabindex','3');
     
     var hbox = document.createElement("hbox");
     hbox.setAttribute('style',"padding-top:10px;");
     hbox.appendChild(saybox);
     hbox.appendChild(saybutton);
     
     lowerBox.appendChild(hbox);
     lowerBox.appendChild(shareq);
     
     mainBox.appendChild(lowerBox);
     
     // Initialize variables
     
     saybox=document.getElementById('saybox');
     
     // END initialization
     
     
     //query=prefs.getCharPref("query");
     //query=p_query;
     usernamePref=prefs.getCharPref("username");
     
     //chatbox.getElementsByTagName("body")[0].style.backgroundColor="#00ee44";   // this is a test! And it works!
     
     var status;
     
     if(!usernamePref || usernamePref=="")   // too lazy to check which is proper way in js...
     {
          alert("Please set extensions.searchparty.username string in about:config");
          window.close();
     }
     
     try
     {
          username = server.searchparty.connect(usernamePref);   // should actually connect at firefox startup
          connectedFlag = true;
     }
     catch(e)  // should actually check for error code, ie already connected, etc...
     {
          reportException(e);
          window.close();
     }
     
     try
     {
          status = server.searchparty.join(username,query,options,lastId)
     }
     catch(e)
     {
          reportException(e);
          window.close();
     }
     
     roomName = status["roomName"];
     
     document.getElementById("searchparty-window").setAttribute("title","Search Party -- "+roomName);
     
     handleRoomStatus(status);
     
     saybox.focus();
}

function closeChatWindow()
{
     var code;
     
     if(connectedFlag == true)
     {
          try
          {
               code=server.searchparty.disconnect(username);     // should check return code, probably part() is more appropriate here...
          }
          catch(e)
          {
               reportException(e);
          }
     }
}

function talk()
{

     var status;
     if(saybox.value!="")
     {
          try
          {
              status=server.searchparty.say(username,roomName,saybox.value,lastId);      
          }
          catch(e)
          {
             reportException(e);
             window.close();
          }
          
          handleRoomStatus(status);
          
          saybox.value="";   
     }
}

function shareState()    // called whenever share checkbox is modified
{
	if(document.getElementById('shareq').checked)
	{
		options="";
	}
	else
	{
		options="NOSHOW";
	}
     
     try
     {
         status=server.searchparty.updateOptions(username,roomName,options,lastId);      
     }
     catch(e)
     {
        reportException(e);
        window.close();
     }
     
     handleRoomStatus(status);
     
}


function refreshChat()
{     
          try
          {
              status=server.searchparty.getUpdate(username,roomName,lastId)      
          }
          catch(e)
          {
             reportException(e);
             window.close();
          }
          
          handleRoomStatus(status);
}

function recommend(url,title,comment)
{
     //alert(url + "\n" + title);
     
     try
     {
         status=server.searchparty.recommend(username,roomName,url,title,comment,lastId)      
     }
     catch(e)
     {
        reportException(e);
        window.close();
     }
     
     handleRoomStatus(status);
}


function hash(name)
{
     var hashval=0;
     for(var i=0; i<name.length; i++)
     {
          hashval+=name.charCodeAt(i);
     }
     return (hashval%7 + 1);
}


/*function loadRecWindow()
{
     var t2 = window.opener.title;
     alert(t2);
     document.getElementByID("rec-window").setAttribute("title","from the js");
    // var title2 = opener.document.getElementsByTagName("title")[0].value;
    // alert(title2);
    // window.title=title2;
}*/
