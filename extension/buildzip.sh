#!/bin/bash

#cd /home/sandy/xul/helloworld.xpi_FILES/chrome/helloworld.jar_FILES/
cd searchparty.xpi_FILES/chrome/searchparty.jar_FILES/

\rm content/searchparty/*~
\rm content/searchparty/js/*~

\rm ../searchparty.jar

zip -r ../searchparty.jar *

cd ..

\rm -r searchparty.jar_FILES

cd ..

\rm *~
\rm ../searchparty.xpi

zip -r ../searchparty.xpi *

cd ..

\rm -r searchparty.xpi_FILES

