from twisted.internet import reactor, defer
from twisted.web.xmlrpc import Proxy

from xml.dom import minidom
import gobject, random, os, datetime
import gtk, gtk.glade, pango, sys, gconf

# i18n
from gettext import gettext as _

from data import get_data_path

gconf_client = gconf.client_get_default()
gconf_client.add_dir("/apps/searchparty", gconf.CLIENT_PRELOAD_RECURSIVE)

class SearchPartyError(Exception):
	pass
	
class ChatRoom(gobject.GObject):
	__gsignals__ = {
		'new-message':			(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
								(gobject.TYPE_PYOBJECT,)),
		'user-left':			(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
								(gobject.TYPE_PYOBJECT,)),
		'user-joined':			(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
								(gobject.TYPE_PYOBJECT,))
	}
	
	def __init__(self, query, username=None):
		gobject.GObject.__init__(self)
		if username == None:
			self._username = gconf_client.get_string("/apps/searchparty/username")
			if self._username==None:
				self._username = "Anonymous"
		else:
			self._username = username
		self._query = query
		self._options = gconf_client.get_string("/apps/searchparty/options")
		self._lastid = ""		
		self._name = "Unknown"
		self._users = []
		self._messages = []
		self._refresh_id = 0
		self._connected = False
		
		self._proxy = Proxy(gconf_client.get_string("/apps/searchparty/server").get_string())
		
		gconf_client.notify_add("/apps/searchparty/options", lambda x,y,z,a: self._on_options_change(z.value))
		gconf_client.notify_add("/apps/searchparty/username", lambda x,y,z,a: self._on_username_change(z.value))
		gconf_client.notify_add("/apps/searchparty/server", lambda x,y,z,a: self._on_server_change(z.value))
	
	def get_username(self):
		return self._username
	def get_query(self):
		return self._query
	def get_name(self):
		return self._name
	def get_users(self):
		return self._users
	def get_messages(self):
		return self._messages
		
	def get_count(self):
		return self._proxy.callRemote('searchparty.count', self._query)
	
	def join(self):
		if self._connected:
			return defer.fail(SearchPartyError("Already Connected"))
			
		return self._proxy.callRemote('searchparty.connect', self._username).addCallback(self._on_connection)
	
	def close(self):
		if not self._connected:
			return defer.succeed("Already Disconnected")
			
		gobject.source_remove(self._refresh_id)
		return self._proxy.callRemote('searchparty.disconnect', self._username).addCallback(self._on_close)
	
	def preview(self):
		return self._proxy.callRemote('searchparty.getUpdate', self._query).addCallback(self._parse_room_status)

	def change_options(self, options):
		if not self._connected:
			return defer.fail(SearchPartyError("Not Connected"))
			
		self._options = options
		return self._proxy.callRemote('searchparty.updateOptions', self._username, self._name, options, self._lastid).addCallback(self._parse_room_status)

	def talk(self, text):
		if not self._connected:
			return defer.fail(SearchPartyError("Not Connected"))
			
		return self._proxy.callRemote('searchparty.say', self._username, self._name, text, self._lastid).addCallback(self._parse_room_status)

	def refresh(self):
		if not self._connected:
			return defer.fail(SearchPartyError("Not Connected"))
			
		return self._proxy.callRemote('searchparty.getUpdate', self._username, self._name, self._lastid).addCallback(self._parse_room_status)

	def recommend(self, url, title, comment):
		if not self._connected:
			return defer.fail(SearchPartyError("Not Connected"))
			
		return self._proxy.callRemote('searchparty.recommend', self._username, self._name, url, title, comment, self._lastid).addCallback(self._parse_room_status)
	
	# Private ----------------------
	def _on_connection(self, username):
		self._username = username
		return self._proxy.callRemote('searchparty.join', self._username, self._query, self._options, self._lastid).addCallback(self._on_join)
	
	def _on_join(self, status):
		self._name = status["roomName"]
		self._parse_room_status(status)
		self._refresh_id = gobject.timeout_add(3000, self._chat_refresh)
		self._connected = True
		return self
	
	def _on_close(self, status):
		self._connected = False
		return self
		
	def _chat_refresh(self):
		if self._connected:
			self.refresh()
			
		return True
	
	def _on_options_change(self, value):
		if value != None and value.type == gconf.VALUE_STRING:
			change_options(self, value.get_string())
	
	def _on_username_change(self, value):
		if value != None and value.type == gconf.VALUE_STRING:
			def update_username(res):
				self._username = value.get_string()
				
			self.close().addCallback(update_username).addCallback(lambda x: self.join())
	
	def _on_server_change(self, value):
		if value != None and value.type == gconf.VALUE_STRING:
			def update_server(res):
				self._proxy = Proxy(value.get_string())
			
			self.close().addCallback(update_server).addCallback(lambda x: self.join())
			
	def _parse_room_status(self, status):
		# Update User list
		users = []
		for user in status["userList"]:
			users.append(user.split(":"))

		if self._users != users:
			# Detect new users
			for newuser in users:
				if not newuser in self._users:
					gobject.GObject.emit(self, 'user-joined', newuser)
					
			# Detect removed users
			for olduser in self._users:
				if not olduser in users:
					gobject.GObject.emit(self, 'user-left', olduser)
					
			self._users = users

		# Update chat room content
		if status["currentChat"] != "<chatFragment></chatFragment>":
			doc = minidom.parseString(status["currentChat"])
			messages = doc.getElementsByTagName("message")

			for message in messages:
				sender = message.getElementsByTagName("sender")[0].firstChild.data.encode('utf8')
				time = message.getElementsByTagName("time")[0].firstChild.data.encode('utf8')
				body = message.getElementsByTagName("body")[0]

				if body.hasChildNodes():
					body = body.firstChild.data.encode('utf8')
				else:
					body = ""

				url = message.getElementsByTagName("url")[0]
				title = ""
				if url.hasChildNodes():
					title = url.getAttribute("title").encode('utf8')
					url = url.firstChild.data.encode('utf8')
				else:
					url = ""

				self._lastid = message.getAttribute("id").encode('utf8')
				
				msg = {
					"sender" : sender,
					"time" : time,
					"body" : body,
					"url" : url,
					"title" : title}
					
				self._messages.append(msg)
				gobject.GObject.emit(self, 'new-message', msg)
				
gobject.type_register(ChatRoom)

class ChatRoomUI(gobject.GObject):
	TARGET_TYPE_TEXT = 80
	__gsignals__ = {
		'closed':	(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
					())
	}
		
	def __init__(self, closeOnExit=False):
		gobject.GObject.__init__(self)
		self.chatroom = None
		
		self.user_colors = {}
		
		self.closeOnExit = closeOnExit
		
		#Glade stuff
		self.glade = gtk.glade.XML(get_data_path('searchparty.glade'), root="searchparty")
		self.window = self.glade.get_widget("searchparty")
		self.entry = self.glade.get_widget("entry")
		self.send = self.glade.get_widget("send")
		self.userlist = self.glade.get_widget("userlist")
		self.users = self.glade.get_widget("users")
		self.chat = self.glade.get_widget("chat")
		
		self.glade = gtk.glade.XML(get_data_path('searchparty.glade'), root="recommenddialog")
		self.recommenddialog = self.glade.get_widget("recommenddialog")
		self._recommend = self.glade.get_widget("recommend")
		self.cancel = self.glade.get_widget("cancel")
		self.title = self.glade.get_widget("title")
		self.comment = self.glade.get_widget("comment")
		
		# Widgets Signals
		self.entry.connect("activate", self._on_send)
		self.send.connect("clicked", self._on_send)
		#self.window.connect("destroy", self._on_close)
		self.window.connect("delete-event", lambda x,y: self._on_close(x))
		self._recommend.connect("clicked", self._on_recommend)
		self.cancel.connect("clicked", lambda x: self.recommenddialog.hide())
		
		#Setup textbuffer
		buf = self.chat.get_buffer()
		buf.create_tag("bold", weight=pango.WEIGHT_BOLD)
		buf.create_tag("small", scale=pango.SCALE_SMALL)
		
		self.hand_cursor = gtk.gdk.Cursor(gtk.gdk.HAND2)
		
		# SEtup userlist view
		self.userlist.insert_column_with_data_func(-1, "Users", gtk.CellRendererText(), self._on_cell_data)
		
		# Setup DND
		for widget in [self.window, self.entry, self.chat, self.userlist]:
			widget.connect('drag-data-received', self._on_drag_receive)
			widget.drag_dest_set(gtk.DEST_DEFAULT_MOTION |
								gtk.DEST_DEFAULT_HIGHLIGHT |
								gtk.DEST_DEFAULT_DROP,
								[("text/plain", 0, self.TARGET_TYPE_TEXT)],
								gtk.gdk.ACTION_COPY)
		
		self._reset_ui()
		
	def show_query(self, query, username=None):
		if query == None:
			return
		if self.chatroom != None:
			self.chatroom.close()
			self._reset_ui()
			
		print 'Creating chatroom:', query
		self.chatroom = ChatRoom(query, username)
		
		# ChatRoom Signals
		self.chatroom.connect('new-message', self._on_new_message)
		self.chatroom.connect('user-joined', self._on_user_joined)
		self.chatroom.connect('user-left', self._on_user_left)
		
		# Start !
		self.chatroom.join().addCallback(self._joined)
		self.present()
	
	def recommend(self, url, title=""):
		self.title.set_text(title)
		self.comment.get_buffer().set_text("")
		self.recommenddialog.url = url
		self.recommenddialog.present()
		self.title.grab_focus()
	
	def present(self):
		self.entry.grab_focus()
		self.window.present()
		
	def _reset_ui(self):
		# Create UserList
		store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
		self.userlist.set_model(store)
		self.users.set_text(_("Connecting"))
		
		# Disable everything until we are connected
		self.entry.set_sensitive(False)
		self.send.set_sensitive(False)
		self.userlist.set_sensitive(False)
		self.chat.set_sensitive(False)
		
		# Setup textbuffer
		buf = self.chat.get_buffer()
		buf.set_text("")
		
		self._update_count()
		
	def _append(self, text, *args):
		buf = self.chat.get_buffer()
		buf.insert_with_tags_by_name(buf.get_iter_at_mark(buf.get_insert()), text, *args)
		self.chat.scroll_to_mark(buf.get_insert(), 0.2)
	
	def _on_drag_receive(self, chat, drag_context, x, y, selection, target_type, time):
		print 'Drag Receive'
		if target_type == self.TARGET_TYPE_TEXT:
			self.recommend(selection.data)
	
	def _on_recommend(self, button):
		title = self.title.get_text()
		url = self.recommenddialog.url
		buf = self.comment.get_buffer()
		comment = buf.get_text(buf.get_start_iter(), buf.get_end_iter(), False)
		
		self.chatroom.recommend(url, title, comment)
		self.recommenddialog.hide()
		
	def _on_close(self, window):
		if self.closeOnExit:
			self.chatroom.close().addBoth(lambda x: reactor.stop())
			return False
		else:
			self.chatroom.close()
			self.chatroom = None
			self.window.hide()
			self._reset_ui()
			gobject.GObject.emit(self, 'closed')
			return True
		
	def _on_send(self, button):
		text = self.entry.get_text()
		if text.startswith("/nick "):
			gconf_client.set_string("/apps/searchparty/username", text[6:])
		else:
			self.chatroom.talk(text)
		self.entry.set_text("")
		
	def _update_count(self):
		if count == 1:
			self.users.set_markup(_("<b>1</b> user"))
		else:
			self.users.set_markup(_("<b>%d</b> users") % len(self.userlist.get_model()))
		
	def _joined(self, chat):
		self.entry.set_sensitive(True)
		self.send.set_sensitive(True)
		self.userlist.set_sensitive(True)
		self.chat.set_sensitive(True)
		
		self.window.set_title(_("Search Party - %s") % self.chatroom.get_query())
		self.present()
		
	def _on_new_message(self, chat, msg):
		self._append(self._get_pretty_timestamp(msg["time"]), "small")
		if msg["url"] != "":
			# Create a text tag for this link
			buf = self.chat.get_buffer()
			tag = buf.get_tag_table().lookup(msg["url"])
			if tag == None:
				tag = buf.create_tag(msg["url"], underline="single", foreground="blue")
				tag.connect('event', self._on_link_event)
			
			self._append(msg["sender"], "bold", self._get_user_color(msg["sender"]))
			self._append(_(" recommends "))
			self._append("%s" % msg["title"], msg["url"])
			
			if msg["body"] != "":
				self._append("\n\t%s" % msg["body"])
		else:
			self._append(msg["sender"], "bold", self._get_user_color(msg["sender"]))
			self._append(": %s" % msg["body"])
		self._append("\n\n")
	
	def _on_link_event(self, tag, widget, event, iter):
		url = tag.get_property("name")
		print 'Tag event', url
		#Fixme integration here ?
		os.spawnlp(os.P_NOWAIT, "gnome-open", "gnome-open", url)
			
	def _on_cell_data(self, column, cell, model, iter):
		cell.set_property('markup',
			#translators: 1) do not change, 2)User name, 3) query term
			_("<b><span foreground='%s'>%s</span></b>\nSearches for '%s'") % (
			self._get_user_color(model.get_value(iter, 0)), model.get_value(iter, 0), model.get_value(iter, 1)))
	
	def _on_user_joined(self, chat, user):
		name, query = user
		#translators: 1)Username, 2)Query term
		self._append(_("%s has joined, he is searching for '%s'") % (name, query) +"\n\n", "bold")
		self.userlist.get_model().append(user)
		self._update_count()
		
	def _on_user_left(self, chat, user):
		self._append(_("%s has quit the chat") % (user[0]) +"\n\n", "bold")
		model = self.userlist.get_model()
		it = model.get_iter_first()
		while it != None:
			if model.get_value(it, 0) == user[0]:
				model.remove(it)
				break
			it = model.iter_next(it)
		
		self._update_count()
		
	def _get_user_color(self, user):
		try:
			return self.user_colors[user]
		except KeyError:
			col = "#%02x%02x%02x" % (random.randint(30, 230), random.randint(30, 230), random.randint(30, 230))
			self.chat.get_buffer().create_tag(col, foreground=col)
			self.user_colors[user] = col
			return col
	
	def _get_pretty_timestamp(self, time):
		date = datetime.datetime.fromtimestamp(long(time))
		now = datetime.datetime.now()
		diff = now - date
		if diff.days == 0:
			#translators: 1) Hour 2) Minutes
			return _("Today, %02d:%02d") % (date.hour, date.minute) +"\n"
		elif diff.days == 1:
			#translators: 1) Hour 2) Minutes
			return _("Yesterday, %02d:%02d") % (date.hour, date.minute)	+"\n"
		elif diff.days >= 2 and diff.days < 7:
			#translators: 1) Days, 2) Hour 3) Minutes
			return _("%d days ago, %02d:%02d") % (diff.days, date.hour, date.minute) +"\n"
		elif diff.days >= 7 and diff.days < 14:
			return _("One week ago")+"\n"
		else:
			return _("%d weeks ago") % (diff.days/7) +"\n"
