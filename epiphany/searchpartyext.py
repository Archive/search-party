from searchparty.mygtk2reactor import install
install(useGtk=True, useExistingGtk=True)

from twisted.internet import reactor
import urllib, re
import gtk, gtk.glade
from gettext import gettext as _

from searchparty.chatroom import ChatRoomUI
import epiphany

# A List of matches to perform against the URL
# A tuple containing (name, a compiled regular expression, A list of fields to extract, in pref order)
QUERY_MATCHES = [
	# Google matches
	(_("Google"), re.compile(".*google\.[a-zA-Z0-9-.]+/.*\?.*q=.*"), ["q"]),
	# Bugzilla
	(_("Gnome Bugzilla"), re.compile(".*bug\w+.gnome.org/.*\?.*(value0-0-0|short_desc)=.*"), ["short_desc","value0-0-0"]),
]
global attached_windows
attached_windows = []
global search_party_active
search_party_active = False
global search_party_valid
search_party_valid = False
global search_party_window
search_party_window = ChatRoomUI()

ui_str = """
<ui>
  <menubar name="menubar">
    <menu name="ToolsMenu" action="Tools">
      <separator/>
      <menuitem name="SearchParty" action="SearchParty"/>
      <menuitem name="SearchPartyRecommend" action="SearchPartyRecommend"/>
      <separator/>
    </menu>
  </menubar>
</ui>
"""

def on_search_party_close(chat, window):
	global search_party_active
	search_party_active = False
	
	group = window.search_party_window_data[0]
	sync_sensitivity(group.get_action ("SearchParty"), group.get_action ("SearchPartyRecommend"))

def on_content_change(embed, url, window):
	global search_party_valid
	
	# If we are not the active tab, there is no point updating
	if window.get_active_tab() != embed.get_parent():
		return
		
	# If the url is a search-party-able one, set the flag
	query = extract_query(url)
	if query == None:	
		search_party_valid = False
	else:
		search_party_valid = True
	
	group = window.search_party_window_data[0]
	sync_sensitivity(group.get_action ("SearchParty"), group.get_action ("SearchPartyRecommend"))
		
def join_search_party(action, window):
	global search_party_active, search_party_window
	embed = window.get_active_embed()
	
	# Show the chatroom
	query = extract_query(embed.get_location(False))
	search_party_window.show_query(query)
		
	search_party_active = True

	#Update item sensivity
	group = window.search_party_window_data[0]
	sync_sensitivity(group.get_action ("SearchParty"), group.get_action ("SearchPartyRecommend"))
	
def recommend_search_party(action, window):
	global search_party_window
	
	embed = window.get_active_embed()
	search_party_window.recommend(embed.get_location(False), embed.get_title())

def on_switch_tab(window, spec):
	embed = window.get_active_embed()
	on_content_change(embed, embed.get_location(False), window)
	
def sync_sensitivity(join_action, recommend_action):
	join_action.set_sensitive((not search_party_active) and search_party_valid)	
	recommend_action.set_sensitive(search_party_active)

def extract_query(url):
	if url == None:	
		return None
	
	print 'Visiting', url
	for name, reg, fields in QUERY_MATCHES:
		if reg.match(url) == None:
			print 'Not %s: %s' % (name, reg.pattern)
			continue
		
		path, query = urllib.splitquery(url)
		if query == None:
			return None
			
		params = query.split("&")
		for param in params:
			for field in fields:
				if param.startswith(field+"="):
					return urllib.unquote_plus(param[len(field)+1:])
			
		return None

actions = [
('SearchParty', None, _('Join Search Party'), None, None, join_search_party),
('SearchPartyRecommend', None, _('Recommend This Site'), None, None, recommend_search_party)
]

def attach_window(window):
	global attached_windows
	if len(attached_windows) <= 0:
		print 'Seems like it we are attached at the first window'
		reactor.run()
	attached_windows.append(window)
	
	# Build menu items
	ui_manager = window.get_ui_manager()
	group = gtk.ActionGroup('SearchParty')
	group.add_actions(actions, window)
	ui_manager.insert_action_group(group, 0)
	ui_id = ui_manager.add_ui_from_string(ui_str)
	
	# When the search party closes, we change item sensivity
	sig1 = search_party_window.connect('closed', on_search_party_close, window)
	#When tab change update availability of search party
	sig2 = window.connect("notify::active-tab", on_switch_tab)
				
	#Remember the menu item we added
	window.search_party_window_data = (group, ui_id, sig1, sig2)
	
	
# Epiphany API
def detach_window(window):
	global attached_windows
	
	# Retreive and remove window data
	group, ui_id, sig1, sig2 = window.search_party_window_data
	del window.search_party_window_data
	
	# Remove menu items
	ui_manager = window.get_ui_manager()
	ui_manager.remove_ui(ui_id)
	ui_manager.remove_action_group(group)
	ui_manager.ensure_update()
	
	# Disconnect closed and active-tab signals
	search_party_window.disconnect(sig1)
	window.disconnect(sig2)
	
	# Check if we get detached from the last window, and stop the reactor.
	attached_windows.remove(window)
	if len(attached_windows) <= 0:
		print 'Seems like we have lost all windows, stop the reactor'
		reactor.stop()

def attach_tab(window, tab):
	embed = tab.get_embed()
	# We want to be notified when the url change to update menu items
	change_id = embed.connect("ge-content-change", on_content_change, window, tab)
	# Store the signal id to disconnect
	tab.search_party_data = (change_id)

def detach_tab(window, tab):
	(change_id) = tab.search_party_data
	del tab.search_party_data
	
	#Remove url change signal
	embed = tab.get_embed()
	embed.disconnect(change_id)
	
